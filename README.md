# Lights
Gradient computation from IES data. The usage of the program is pretty straightforward, just change the filename in `fileName` and run. Program allows to choose vertical angles, automaticaly recomputes their respective positions towards center and in the same time evaluates where the center point most likely is. This part is not necessarily precise because of the two-times interpolation which can shift `[0,0]` between two pixels.

Program can generate `csv` file of the interpolated/approximated values via method `save_to_csv` on demand.

# Dependencies
No unusual dependencies required.

# Files
* `IES-approximation` - computation using spline interpolation which changes input values from IES file
* `IES-interpolation` - computation using bi-linear interpolation which keeps original values and adds the new ones only where missing
* `Gradient_fromVarroc` - demo files and presentation with the ground truth values from Varroc

# Gradient computation
The formula behind the main outcome is log-based computation following IEEE standart.

```
for y in range(0, height - 1):
     valueUp = ies[y, column]
     valueDown = ies[y + 1, column]
     gradient = log10(valueDown) - log10(valueUp)
     if (gradient > maxG):
         maxG = gradient
         maxGRow = y
```

Computed values compared with the ground thruth are available here:
https://docs.google.com/spreadsheets/d/1oKRZdpEG1YA6ojLCznvDEzvtZx6zNdxP30I2Bz7qVrU/edit?usp=sharing
